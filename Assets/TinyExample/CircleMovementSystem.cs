﻿using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;

/// <summary>
/// Circle movement system.
/// Responsible for moving object around
/// </summary>
public class CircleMovementSystem : SystemBase
{
    protected override void OnUpdate()
    {
        // Get time
        var t = (float)Time.ElapsedTime;

        // Move each entity with CircleMovementComponent
        Entities.ForEach((ref Translation pos, ref CircleMovementComponent cm) =>
        {
            // Assign position on a circle
            pos.Value = new float3(math.sin(t * cm.Speed), 0, math.cos(t * cm.Speed)) * cm.PositionMultiplier;
        }).ScheduleParallel();
    }
}

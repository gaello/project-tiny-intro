﻿using Unity.Entities;

/// <summary>
/// Circle movement component.
/// Store component configuration.
/// </summary>
[GenerateAuthoringComponent]
public struct CircleMovementComponent : IComponentData
{
    public float PositionMultiplier;
    public float Speed;
}
